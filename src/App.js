import './App.css'
import CarouselSlider from './components/CarouselSlider'
import Main from './components/Main'
import Map from './components/Map'
import Navigation from './components/Navigation'

function App() {
  return (
    <div className="App">
      <Navigation />
      <CarouselSlider />
      <Main />
    </div>
  )
}

export default App
