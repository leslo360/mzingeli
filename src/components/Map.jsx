import React from 'react'
import GoogleMapReact from 'google-map-react'
import mapStyle from './googleMapsStyle'
import './Map.css'
import { Phone, Email, PhoneAndroid} from '@material-ui/icons'
import wave from '../img/hand.gif'

const Marker = ({ street, town, city }) => (
  <div className="marker">
    <div className="marker__title">
      <span className="wave">
        <img src={wave} />
      </span>
      <h1>
        <i className="call">call...</i>
        <i className="email">email or...</i>
        <i className="visit">visit us...</i>
      </h1>
    </div>
    <div className="contact__info">
      <h2>
        <PhoneAndroid />
        (+27)65-888-2150
      </h2>
      <h2>
        <Phone />
        (+27)11-435-0969
      </h2>
      <h3>
        <Email />
        info@umzingeli.co.za
      </h3>
    </div>
    <div className="marker__info">
      <h4>{street}</h4>
      <h6>{town}</h6>
      <h6>{city}</h6>
    </div>
  </div>
)

const Map = () => {
  const location = {
    center: {
      lat: -26.26527217763312,
      lng: 28.05233524309908,
    },
    zoom: 16,
  }
  const mapOptions = (maps) => {
    return {
      styles: mapStyle,
      disableDefautUI: true,
    }
  }

  return (
    <div style={{ height: '500px', width: '900px' }} className="map">
      <GoogleMapReact
        options={mapOptions}
        bootstrapURLKeys={{ key: 'AIzaSyAHVs0sxo5nvlwi2sKbyH_K6g6UOBjxNGg' }}
        defaultCenter={location.center}
        defaultZoom={location.zoom}
        yesIWantToUseGoogleMapApiInternals
      >
        <Marker
          lat={-26.26527217763312}
          lng={28.05233524309908}
          street="11 Letaba Road"
          town="Eastcliff"
          city="Johanessburg South"
        />
      </GoogleMapReact>
    </div>
  )
}

export default Map
