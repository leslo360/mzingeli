import React, { useEffect, useState } from 'react'
import {
  Button,
  Form,
  FormControl,
  Nav,
  Navbar,
  NavDropdown,
} from 'react-bootstrap'
import { Call, Email, PhoneAndroid } from '@material-ui/icons'
import './navigation.css'

const Navigation = () => {
  const [show, handleShow] = useState(false)

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 100) {
        handleShow(true)
      } else handleShow(false)
    })
    return () => {
      window.removeEventListener('scroll')
    }
  }, [])

  return (
    <Navbar
      bg={`${show ? 'light' : 'transparent'}`}
      expand="lg"
      className="navbar"
    >
      <Navbar.Brand href="#home" className="navbar__brand">
        <>
          <PhoneAndroid />
          <a href="#" target="_blank" className="navbar__brandLink">
            065-888-2150
          </a>
        </>
        <>
          <Call />
          <a href="#" target="_blank" className="navbar__brandLink">
            011-435-0969
          </a>
        </>
        <>
          <Email />
          <a href="#" target="_blank" className="navbar__brandLink">
            INFO@UMZINGELI.CO.ZA
          </a>
        </>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto ">
          <Nav.Link href="#home" className="navbar__navLink">
            Home
          </Nav.Link>
          <Nav.Link href="#about" className="navbar__navLink">
            About Us
          </Nav.Link>
          <Nav.Link href="#solutions" className="navbar__navLink">
            Our Solutions
          </Nav.Link>
          <Nav.Link href="#partner" className="navbar__navLink">
            Why Partner With Us?
          </Nav.Link>
          <Nav.Link href="#contact" className="navbar__navLink">
            Contact us
          </Nav.Link>
          <Nav.Link href="#" className="navbar__navLink">
            Articles
          </Nav.Link>
          {/* <Nav.Link href="#link"></Nav.Link> */}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default Navigation
