import React from 'react'
import './partner.css'
import { BubbleChart } from '@material-ui/icons'
import WhyPartner from '../img/portfolio/7612-3.png'

const Partner = () => {
  return (
    <div className="partner" id="partner">
      <div className="partner__title">
        <h1>Why Partner with us?</h1>
      </div>
      <div className="partner__body">
        <div className="partner__body__info">
          <p>
            Organisations choose Mzingeli when they are tired of the way we've
            always done it or realize they need to be challenged to achieve
            meaningful and impactful change underpinned by:
          </p>
          <i>
            <BubbleChart />
            <span>
              Customised solutions that respond to client specific challenges
              and/or opportunities.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              Partnership with our clients to deliver pragmatic and high impact
              and yield solutions.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              ROI/Benefits/Expected outcomes clearly defined at
              planning/solution design phase.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              Key Strategic Initiatives managed as projects with agreed
              specifications, timelines and budget.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              Continous focus on Quality Assurance to guarantee High Standards
              of our work.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              Opened to independent verification of Improvement Benefits
              Realised by our clients.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>Upskilling and cross skilling our clients.</span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              Skills transfer is an integral part of everything we do.
            </span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>Leaving things better than when we found them.</span>
            <br />
          </i>
          <i>
            <BubbleChart />
            <span>
              Questioning our own work and identifying new improvement
              opportunities.
            </span>
            <br />
          </i>
        </div>
        <div className="partner__img">
          <img src={WhyPartner} alt="why partner with us" />
        </div>
      </div>
    </div>
  )
}

export default Partner
