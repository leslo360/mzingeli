import React from 'react'
import './solutions.css'
import {
  BarChart,
  ImportantDevices,
  HowToReg,
  Business,
  School,
  Public,
} from '@material-ui/icons'
import Zoom from 'react-img-zoom'
import PortfolioImage from '../img/portfolio/7612-2.png'
import PortfolioImage2 from '../img/portfolio/7612-4.png'
const Solutions = () => {
  return (
    <div className="solutions" id="solutions">
      <div className="solutions__title">
        <h1>Our Solutions</h1>
      </div>
      <div className="solutions__services">
        <p>
          {' '}
          <BarChart />
          Strategy Formulation, Execution, Tracking and Reporting Solutions
        </p>
        <p>
          {' '}
          <ImportantDevices /> Integrated Risk Management Solutions
        </p>
        <p>
          <HowToReg /> Due Diligence / Verification Solutions
        </p>
        <p>
          <Business /> Business / Market Research Solutions
        </p>
        <p>
          <School /> Training and Coaching Solutions
        </p>
        <p>
          <Public /> Digitally / Technologically enabled Solutions
        </p>
      </div>
      <div className="solutions__portfolio">
        <img src={PortfolioImage} alt="port" />
        <img src={PortfolioImage2} alt="port2" />
      </div>
    </div>
  )
}

export default Solutions
