import React from 'react'
import AboutUs from './AboutUs'
import Contact from './Contact'
import Partner from './Partner'
import Solutions from './Solutions'
import './main.css'
const Main = () => {
  return (
    <div className="main">
      <AboutUs />
      <Solutions />
      <Partner />
      <Contact />
    </div>
  )
}

export default Main
