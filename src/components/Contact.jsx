import React from 'react'
import './contact.css'
import Map from './Map'
const Contact = () => {
  return (
    <div className="contact" id="contact">
      <div className="contact__title">
        <h1>Contact Us</h1>
      </div>
      <div className="contact__body">
        <Map />
      </div>
    </div>
  )
}

export default Contact
