import React from 'react'
import './about.css'
import { Image } from 'react-bootstrap'
import uncle from '../img/Uncle.jpg'

const AboutUs = () => {
  return (
    <div className="about" id="about">
      <div className="about__title">
        <h1>About Us</h1>
      </div>

      <div className="about__body">
        <div className="about__body__info">
          <p>
            {' '}
            MZingeLi MEH Business Performance Improvement Solutions is a
            business performance improvement solutions provider that focuses on
            providing organisations of all sizes with{' '}
            <strong>Strategy & Risk Management Support Services</strong> with a
            specific emphasis on{' '}
            <strong>
              Strategy Development and Execution, Integrated Risk Management,
              Research and Data Analytics, Fraud Risk Management
            </strong>{' '}
            as well as <strong>Training and Coaching</strong> to upskill our
            clients where there are skills deficiences. We focus on challenging
            your status quo, and energising your organisation to the next level.
            We examine the performance of your business from every angle, and
            work with <strong>Senior Management, Executive Committees</strong>{' '}
            and all levels of the organisation to identify improvement
            opportunities and chart a new path forward. With a particular focus
            on{' '}
            <em>
              strategy development and execution, risk management processes,
              evaluation and improvement, proactive risk identification and
              mitigation, digital roadmaps, technology evolution (process
              automation) and disruptive moves.
            </em>{' '}
            This is the team that will take you to a leading position in your
            industry.
          </p>
          <br />
          <p>
            <strong>Our Guiding Principles</strong>
            <q>
              A thing is not right because we do it. A method is not good
              because we use it. Equipment is not the best because we own it
            </q>{' '}
            John Aldair.
            <q>
              We ask questions and question things that work well in order to
              see if they can work even better
            </q>
          </p>
        </div>
        <div className="about__thumbnail">
          <Image src={uncle} alt="uncle" fluid thumbnail rounded />
        </div>
      </div>
    </div>
  )
}

export default AboutUs
