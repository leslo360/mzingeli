import React, { useEffect, useState } from 'react'
import { Carousel } from 'react-bootstrap'

import img1 from '../img/sipapu.jpg'
import img2 from '../img/mountain.jpg'
import img3 from '../img/heron.jpg'

import './carousel.css'

const CarouselSlider = () => {
  const [isThisASmartPhone, setIsSmartPhone] = useState(false)

  useEffect(() => {
    window.navigator.userAgent.includes('iPhone') ||
      (window.navigator.userAgent.includes('Android') && setIsSmartPhone(true))
  }, [])

  console.log(window.navigator.userAgent)
  return (
    <Carousel className="carousel" id="home">
      <Carousel.Item className="carousel__item">
        {isThisASmartPhone ? (
          <img className="d-block " src={img1} alt="First slide" />
        ) : (
          <video
            className="w-100 carousel__video"
            autoPlay="autoplay"
            loop="loop"
            muted="muted"
          >
            <source src={process.env.PUBLIC_URL + 'SouthAfrica.mp4'} />
          </video>
        )}

        <Carousel.Caption className="carousel__caption">
          <h1 className="carousel__captionTitle">
            <u>MZingeLi</u> MEH
          </h1>
          <p className="carousel__captionLead">
            Business Performance Improvement Solutions
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="carousel__item">
        {isThisASmartPhone ? (
          <img className="d-block " src={img2} alt="First slide" />
        ) : (
          <video
            className="w-100 carousel__video"
            autoPlay="autoplay"
            loop="loop"
            muted="muted"
          >
            <source src={process.env.PUBLIC_URL + 'River.mp4'} />
          </video>
        )}
        <Carousel.Caption className="carousel__caption">
          <h1 className="carousel__captionTitle">Our Vision </h1>
          <p className="carousel__captionLead">
            We will be the Global Leader in providing the most innovative and creative digitally enabled World Class Business Performance improvement Solutions that enable Organizations achieve their future Aspirations
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="carousel__item">
        {isThisASmartPhone ? (
          <img className="d-block " src={img3} alt="First slide" />
        ) : (
          <video
            className="w-100 carousel__video"
            autoPlay="autoplay"
            loop="loop"
            muted="muted"
          >
            <source src={process.env.PUBLIC_URL + 'Waves.mp4'} />
          </video>
        )}
        <Carousel.Caption className="carousel__caption">
          <h1 className="carousel__captionTitle">Our Mission</h1>
          <p className="carousel__captionLead">
            Our Purpose is to give our clients a competitive edge by designing
            and implementing Business Performance Improvement Solutions that
            leave things better than we found them
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  )
}

export default CarouselSlider
